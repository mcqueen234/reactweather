import React, { Component } from 'react';
import './App.css';
import Header from './components/layout/Header';
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import SearchCity from './components/SearchCity';
import Weather from './components/Weather';


class App extends Component {

  state = {
    weather: {
      "location": {
        "name": "Tel Aviv-Yafo",
        "region": "Tel Aviv",
        "country": "Israel",
        "lat": 32.07,
        "lon": 34.76,
        "tz_id": "Asia/Jerusalem",
        "localtime_epoch": 1553696507,
        "localtime": "2019-03-27 16:21"
    },
    "current": {
        "last_updated_epoch": 1553696106,
        "last_updated": "2019-03-27 16:15",
        "temp_c": 18.0,
        "temp_f": 64.4,
        "is_day": 1,
        "condition": {
            "text": "Partly cloudy",
            "icon": "//cdn.apixu.com/weather/64x64/day/116.png",
            "code": 1003
        },
        "wind_mph": 13.6,
        "wind_kph": 22.0,
        "wind_degree": 230,
        "wind_dir": "SW",
        "pressure_mb": 1019.0,
        "pressure_in": 30.6,
        "precip_mm": 0.1,
        "precip_in": 0.0,
        "humidity": 73,
        "cloud": 50,
        "feelslike_c": 18.0,
        "feelslike_f": 64.4,
        "vis_km": 10.0,
        "vis_miles": 6.0,
        "uv": 5.0,
        "gust_mph": 17.0,
        "gust_kph": 27.4
    } 
    }
  }

  getStyle = () => {
    return {
      backgroundImage: this.state.weather.current.is_day ? 'linear-gradient(-90deg, #57A6FF, #3B17B3)' : 'linear-gradient(-90deg, #470098, #3B17B3)'
    }
  }

  getWeatherForLocation = (location) => {
    axios.get("https://api.apixu.com/v1/current.json?key=36e8efe611b7492fae173039192703&q="+location).then(res => this.setState({ weather: res.data }));
  }

  componentDidMount () {
    this.ipLookUp();
  }

  ipLookUp () {
    axios.get('http://ip-api.com/json')
    .then(
        function success(response) {
            console.log('User\'s Location Data is ', response);
            console.log('User\'s Country', response.data.city);
            this.getWeatherForLocation(response.data.city);
        }.bind(this),
        function fail(status) {
            console.log('Request failed.  Returned status of', status);
        }
    );
  }

  render() {
    return (
      <Router>
        <div className="App" style={ this.getStyle() }>
          <Header />
          <div className="container">
            <SearchCity getWeatherForLocation={this.getWeatherForLocation}/>
            <Weather weather={this.state.weather} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
