import React, { Component } from 'react';
import AdditionalInfo from './AdditionalInfo';

export class Weather extends Component {

getStyle = () => {
  return {
    background: this.props.weather.current.is_day ? '#57A6FF' : '#3B17B3'
  }
}
getImageSRC = () => {
  return this.props.weather.current.condition.icon.split("/").pop();
}

  render() {
    const { location, current } = this.props.weather;
    const imageSRC = this.props.weather.current.condition.icon;
    const windData = "" + current.wind_dir + current.wind_kph + ' km/hr';
    const precipData = "" + current.precip_mm + ' mm';
    const humidity = "" + current.humidity + '%';
    const feelsLike = "" + current.feelslike_c + '°';
    const pressure = "" + current.pressure_mb + ' hPa';
    const uv = "" + current.uv;
    const visibility = "" + current.vis_km + ' km';
    return (
      <div>
        <div style={conditionLocationStyle}>{location.name}, {location.country}</div>
        <h3 style={{textAlign: 'right', marginRight: '20px', color: '#FFF'}}>{location.localtime}</h3>
        <div style={conditionTextStyle}>{current.condition.text}</div>
        <div style={{display: 'block', height: '160px'}}>
            <div style={tempStyle}>{current.temp_c}°</div>
            <img src ={imageSRC} style={imgStyle}/>
        </div>
        <div style={{color: '#FFF', fontSize: '23px', marginLeft: '20px', marginRight: '20px'}}>

            <AdditionalInfo title={ 'WIND' } data={ windData } />
            <AdditionalInfo title={ 'PRECIPITATION' } data={ precipData } />
            <AdditionalInfo title={ 'HUMIDITY' } data={ humidity } />
            <AdditionalInfo title={ 'FEELS LIKE' } data={ feelsLike } />
            <AdditionalInfo title={ 'PRESSURE' } data={ pressure } />
            <AdditionalInfo title={ 'UV INDEX' } data={ uv } />
            <AdditionalInfo title={ 'VISIBILITY' } data={ visibility } />
        </div>
      </div>
    )
  }
}

const conditionTextStyle = {
    color: '#FFFFFF',
    fontSize: '30px',
    textAlign: 'left',
    marginLeft: '20px',
    marginTop: '25px'
}
const conditionLocationStyle = {
    color: '#FFFFFF',
    fontSize: '45px',
    textAlign: 'right',
    marginLeft: '20px',
    marginRight: '20px',
    marginTop: '20px'
}

const tempStyle = {
    color: '#FFFFFF',
    fontSize: '90px',
    textAlign: 'left',
    marginLeft: '20px',
    float: 'left'
}
const imgStyle = {
    float: 'right',
    height: '128px',
    width: '128px',
    marginRight: '20px',
    marginTop: '10px'
}

export default Weather;