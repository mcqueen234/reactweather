import React, { Component } from 'react'

export class SearchCity extends Component {

    state = {
        location: ''
    }

    onChange = (e) => this.setState({[e.target.name]: e.target.value});

    onSubmit = (e) => {
        e.preventDefault();
        this.props.getWeatherForLocation(this.state.location);
        this.setState({location: ''});
    }

  render() {
    return (
      <form style={{ display: 'flex', height: '45px', marginTop: '10px', marginLeft: '20px', marginRight: '20px' }} onSubmit={this.onSubmit}>
          <input type="text" name="location" placeholder="Search city..." style={{flex: '10', padding: '5px', fontSize: '20px'}} value={this.state.location} onChange={this.onChange} />
          <input type="submit" value="Show" className="btn" style={{flex: '1', fontSize: '20px'}}  />
      </form>
    )
  }
}

export default SearchCity;
