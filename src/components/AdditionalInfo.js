import React, { Component } from 'react'

export class AdditionalInfo extends Component {

  render() {
      const title = this.props.title;
      const data = this.props.data;
    return (
        <div style={{marginTop: '10px', marginBottom: '20px', borderTop: '1px solid #FFF', height: '45px'}}>
            <div style={{float: 'left', marginTop: '10px', marginBottom: '10px'}}>{title}</div>
            <div style={{float: 'right', marginTop: '10px', marginBottom: '10px'}}>{data}</div>
        </div>
    )
  }
}

export default AdditionalInfo;
